import java.util.Random;

public class Map {

	Random randy = new Random();
	double x = 0,
		y = 0,
		r,
		totalDistance;
	
	public Map() {
		
	}
	
	public void calculateLocation() {
		for (int i = 0; i < 12; i++) {
			r = randy.nextInt(4);
			//0 means northern movement, or increase in y value
			if (r == 0) {
				y++;
			}
			//1 means eastern movement, or increase in x value
			if (r == 1) {
				x++;
			}
			//2 means southern movement, or decrease in y value
			if (r == 2) {
				y--;
			}
			//3 means western movement, or decrease in x value
			if (r == 3) {
				x--;
			}
		}
		totalDistance = Math.sqrt(Math.abs(x*x) + Math.abs(y*y));
		double[] points = {x, y};
		System.out.println("End Point: (" + points[0] + ", " + points[1] + ")");
		System.out.println(Math.abs(points[0]*5)+ " Miles along X axis " + Math.abs(points[1]*5) + " Miles along Y axis");
		System.out.println((totalDistance*5) + " Miles" + " or " + totalDistance + " units away");
	}
}