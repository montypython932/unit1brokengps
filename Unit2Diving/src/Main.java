import java.util.Scanner;

public class Main {
	
	public static void main(String[] args) {
		System.out.println(calculateScore(inputAllScores(), inputValidDegreeOfDifficulty()));
	}
	
	public static double[] inputAllScores() {
		double[] judgesScores = new double[7];
		for(int i = 0; i < judgesScores.length;i++) {
			System.out.println("Please input score number " + (i+1));
			judgesScores[i] = inputValidScore();
		}
		return judgesScores;
	}
	
	public static double inputValidScore() {
		Scanner input = new Scanner(System.in);
		double score = input.nextDouble();
		return score;
	}
	
	public static double inputValidDegreeOfDifficulty() {
		System.out.println("Please input degree of difficulty ");
		Scanner input = new Scanner(System.in);
		double difficulty = input.nextDouble();
		return difficulty;
	}
	
	public static double calculateScore(double[] scores, double difficulty) {
		double min =10, 
			   max = 0,
			   total = 0;
		for (int i = 1; i < scores.length; i++) {
			if (scores[i] < scores[i-1]) {
				min = scores[i];
			}
			if (scores[0] < min) {
				min = scores[0];
			}
			if (scores[i] > scores[i-1]) {
				max = scores[i];
			}
			if (scores[0] > max) {
				max = scores[0];
			}
		}
		for (int j = 0; j < scores.length - 1; j++) {
			if (scores[j] != min && scores[j] != max) {
				total = total + scores[j];
			}
		}
		return total * 0.6;
	}
}
